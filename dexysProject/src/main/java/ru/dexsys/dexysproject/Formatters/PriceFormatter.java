package ru.dexsys.dexysproject.Formatters;

import java.util.Locale;

public class PriceFormatter {

    public static String formatPrice (double d, String s) {
        if (s.contains("rur")) {
            return String.format(Locale.CANADA_FRENCH, "%,.2f", d) + " " + "\u20BD";
        } else if (s.contains("usd")) {
            return String.format(Locale.CANADA_FRENCH, "%,.2f", d) + " " + "\u0024";
        } else return null;
    }
}
