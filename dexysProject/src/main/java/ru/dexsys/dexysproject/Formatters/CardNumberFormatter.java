package ru.dexsys.dexysproject.Formatters;

public class CardNumberFormatter {

    public static String formatCardNumber(String s) {
        if (s.startsWith("4")) {
            s = "VISA "+ s.replaceAll("(.{4})", "$1 ");
            return s;
        } else if (s.startsWith("5")) {
            s = "MAESTRO "+ s.replaceAll("(.{4})", "$1 ");
            return s;
        } else return null;
    }
}
