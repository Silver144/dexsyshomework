package ru.dexsys.dexysproject.Formatters;

import java.util.Date;

public class DateFormatter {

    public static String formatDate (Date d) {
        return String.format("%td. %<tm. %<tY", d);
    }
}
