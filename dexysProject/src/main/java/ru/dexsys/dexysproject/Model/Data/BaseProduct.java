package ru.dexsys.dexysproject.Model.Data;

import android.support.annotation.Nullable;

import com.google.gson.annotations.SerializedName;

import java.util.Date;

public class BaseProduct {

    @SerializedName("name")
    private String name;
    @SerializedName("contractNumber")
    private Integer contractNumber;
    @SerializedName("openedDate")
    private Date openedDate;
    @SerializedName("amount")
    private Double amount;
    @SerializedName("currency")
    private String currency;
    @SerializedName("status")
    private String status = "active";

    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }

    public Integer getContractNumber() {
        return contractNumber;
    }
    public void setContractNumber(Integer contractNumber) {
        this.contractNumber = contractNumber;
    }

    public Date getOpenedDate() {
        return openedDate;
    }
    public void setOpenedDate(Date openedDate) {
        this.openedDate = openedDate;
    }

    public Double getAmount() {
        return amount;
    }
    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public String getCurrency() {
        return currency;
    }
    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getStatus() {
        return status == null ? "" : status;
    }
    public void setStatus(@Nullable String status) {
        this.status = status;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        BaseProduct product = (BaseProduct) o;

        if (contractNumber != product.contractNumber) return false;
        if (!name.equals(product.name)) return false;
        if (!openedDate.equals(product.openedDate)) return false;
        return status != null ? status.equals(product.status) : product.status == null;
    }

    @Override
    public int hashCode() {
        int result = name.hashCode();
        result = 31 * result + contractNumber;
        result = 31 * result + openedDate.hashCode();
        result = 31 * result + (status != null ? status.hashCode() : 0);
        return result;
    }

}
