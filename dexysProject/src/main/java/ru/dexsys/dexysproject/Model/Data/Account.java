package ru.dexsys.dexysproject.Model.Data;

import com.google.gson.annotations.SerializedName;

import java.util.Date;

public class Account extends BaseProduct {

    @SerializedName("percent")
    private Double percent;
    @SerializedName("closingDate")
    private Date closingDate;

    private Account(Builder builder) {
        setName(builder.name);
        setContractNumber(builder.contractNumber);
        setOpenedDate(builder.openedDate);
        setAmount(builder.amount);
        setCurrency(builder.currency);
        setStatus(builder.status);
        setPercent(builder.percent);
        setClosingDate(builder.closingDate);
    }

    public static final class Builder {
        private String name;
        private Integer contractNumber;
        private Date openedDate;
        private Double amount;
        private String currency;
        private String status;
        private Double percent;
        private Date closingDate;

        public Builder() {
        }

        public Builder name(String val) {
            name = val;
            return this;
        }
        public Builder contractNumber(Integer val) {
            contractNumber = val;
            return this;
        }
        public Builder openedDate(Date val) {
            openedDate = val;
            return this;
        }
        public Builder amount(Double val) {
            amount = val;
            return this;
        }
        public Builder currency(String val) {
            currency = val;
            return this;
        }
        public Builder status(String val) {
            status = val;
            return this;
        }
        public Builder percent(Double val) {
            percent = val;
            return this;
        }
        public Builder closingDate(Date val) {
            closingDate = val;
            return this;
        }
        public Account build() {
            return new Account(this);
        }
    }

    public Double getPercent() {
        return percent;
    }
    public void setPercent(Double percent) {
        this.percent = percent;
    }

    public Date getClosingDate() {
        return closingDate;
    }
    public void setClosingDate(Date closingDate) {
        this.closingDate = closingDate;
    }
}
