package ru.dexsys.dexysproject.Model.Data;

import com.google.gson.annotations.SerializedName;

import java.util.Date;
import java.util.List;

public class Credit extends BaseProduct {

    @SerializedName("nextPaymentDate")
    private Date nextPaymentDate;
    @SerializedName("nextPaymentAmount")
    private Double nextPaymentAmount;
    @SerializedName("percent")
    private Double percent;
    @SerializedName("payments")
    private List<Payment> payments;
    @SerializedName("closingDate")
    private Date closingDate;

    private Credit(Builder builder) {
        setName(builder.name);
        setContractNumber(builder.contractNumber);
        setOpenedDate(builder.openedDate);
        setAmount(builder.amount);
        setCurrency(builder.currency);
        setStatus(builder.status);
        setNextPaymentDate(builder.nextPaymentDate);
        setNextPaymentAmount(builder.nextPaymentAmount);
        setPercent(builder.percent);
        setPayments(builder.payments);
        setClosingDate(builder.closingDate);
    }

    public static final class Builder {
        private String name;
        private Integer contractNumber;
        private Date openedDate;
        private Double amount;
        private String currency;
        private String status;
        private Date nextPaymentDate;
        private Double nextPaymentAmount;
        private Double percent;
        private List<Payment> payments;
        private Date closingDate;

        public Builder() {
        }

        public Builder name(String val) {
            name = val;
            return this;
        }
        public Builder contractNumber(Integer val) {
            contractNumber = val;
            return this;
        }
        public Builder openedDate(Date val) {
            openedDate = val;
            return this;
        }
        public Builder amount(Double val) {
            amount = val;
            return this;
        }
        public Builder currency(String val) {
            currency = val;
            return this;
        }
        public Builder status(String val) {
            status = val;
            return this;
        }
        public Builder nextPaymentDate(Date val) {
            nextPaymentDate = val;
            return this;
        }
        public Builder nextPaymentAmount(Double val) {
            nextPaymentAmount = val;
            return this;
        }
        public Builder percent(Double val) {
            percent = val;
            return this;
        }
        public Builder payments(List<Payment> val) {
            payments = val;
            return this;
        }
        public Builder closingDate(Date val) {
            closingDate = val;
            return this;
        }
        public Credit build() {
            return new Credit(this);
        }
    }

    public Date getNextPaymentDate() {
        return nextPaymentDate;
    }
    public void setNextPaymentDate(Date nextPaymentDate) {
        this.nextPaymentDate = nextPaymentDate;
    }

    public Double getNextPaymentAmount() {
        return nextPaymentAmount;
    }
    public void setNextPaymentAmount(Double nextPaymentAmount) {
        this.nextPaymentAmount = nextPaymentAmount;
    }

    public Double getPercent() {
        return percent;
    }
    public void setPercent(Double percent) {
        this.percent = percent;
    }

    public List<Payment> getPayments() {
        return payments;
    }
    public void setPayments(List<Payment> payments) {
        this.payments = payments;
    }

    public Date getClosingDate() {
        return closingDate;
    }
    public void setClosingDate(Date closingDate) {
        this.closingDate = closingDate;
    }
}
