package ru.dexsys.dexysproject.Model.Data;

import com.google.gson.annotations.SerializedName;

import java.util.Date;

public class CreditCard extends BaseProduct {

    @SerializedName("cardNumber")
    private String cardNumber;
    @SerializedName("overdueDate")
    private Date overdueDate;
    @SerializedName("imageUrl")
    private String imageUrl;
    @SerializedName("closingDate")
    private Date closingDate;

    private CreditCard(Builder builder) {
        setName(builder.name);
        setContractNumber(builder.contractNumber);
        setOpenedDate(builder.openedDate);
        setAmount(builder.amount);
        setCurrency(builder.currency);
        setStatus(builder.status);
        setCardNumber(builder.cardNumber);
        setOverdueDate(builder.overdueDate);
        setImageUrl(builder.imageUrl);
        setClosingDate(builder.closingDate);
    }

    public static final class Builder {
        private String name;
        private Integer contractNumber;
        private Date openedDate;
        private Double amount;
        private String currency;
        private String status;
        private String cardNumber;
        private Date overdueDate;
        private String imageUrl;
        private Date closingDate;

        public Builder() {
        }

        public Builder name(String val) {
            name = val;
            return this;
        }
        public Builder contractNumber(Integer val) {
            contractNumber = val;
            return this;
        }
        public Builder openedDate(Date val) {
            openedDate = val;
            return this;
        }
        public Builder amount(Double val) {
            amount = val;
            return this;
        }
        public Builder currency(String val) {
            currency = val;
            return this;
        }
        public Builder status(String val) {
            status = val;
            return this;
        }
        public Builder cardNumber(String val) {
            cardNumber = val;
            return this;
        }
        public Builder overdueDate(Date val) {
            overdueDate = val;
            return this;
        }
        public Builder imageUrl(String val) {
            imageUrl = val;
            return this;
        }
        public Builder closingDate(Date val) {
            closingDate = val;
            return this;
        }
        public CreditCard build() {
            return new CreditCard(this);
        }
    }


    public String getCardNumber() {
        return cardNumber;
    }
    public void setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
    }

    public Date getOverdueDate() {
        return overdueDate;
    }
    public void setOverdueDate(Date overdueDate) {
        this.overdueDate = overdueDate;
    }

    public String getImageUrl() {
        return imageUrl;
    }
    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public Date getClosingDate() {
        return closingDate;
    }
    public void setClosingDate(Date closingDate) {
        this.closingDate = closingDate;
    }
}
