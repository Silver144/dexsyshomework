package ru.dexsys.dexysproject.Model.Data;

import com.google.gson.annotations.SerializedName;

import java.util.Date;

public class DebitCard extends BaseProduct {

    @SerializedName("cardNumber")
    private String cardNumber;
    @SerializedName("imageUrl")
    private String imageUrl;

    private DebitCard(Builder builder) {
        setName(builder.name);
        setContractNumber(builder.contractNumber);
        setOpenedDate(builder.openedDate);
        setAmount(builder.amount);
        setCurrency(builder.currency);
        setStatus(builder.status);
        setCardNumber(builder.cardNumber);
        setImageUrl(builder.imageUrl);
    }

    public static final class Builder {
        private String name;
        private Integer contractNumber;
        private Date openedDate;
        private Double amount;
        private String currency;
        private String status;
        private String cardNumber;
        private String imageUrl;

        public Builder() {
        }

        public Builder name(String val) {
            name = val;
            return this;
        }
        public Builder contractNumber(Integer val) {
            contractNumber = val;
            return this;
        }
        public Builder openedDate(Date val) {
            openedDate = val;
            return this;
        }
        public Builder amount(Double val) {
            amount = val;
            return this;
        }
        public Builder currency(String val) {
            currency = val;
            return this;
        }
        public Builder status(String val) {
            status = val;
            return this;
        }
        public Builder cardNumber(String val) {
            cardNumber = val;
            return this;
        }
        public Builder imageUrl(String val) {
            imageUrl = val;
            return this;
        }
        public DebitCard build() {
            return new DebitCard(this);
        }
    }

    public String getCardNumber() {
        return cardNumber;
    }
    public void setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
    }

    public String getImageUrl() {
        return imageUrl;
    }
    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }
}
