package ru.dexsys.dexysproject.Model.Repository;

import android.support.annotation.NonNull;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import ru.dexsys.dexysproject.Model.Data.BaseProduct;
import ru.dexsys.dexysproject.Parsing.ProductsResponse;

public class BaseProductConverter {
    private static final String TAG = "FragLog";

    public BaseProductConverter() {
        //конструктор
    }

    @NonNull
    public static List<BaseProduct> addListsToBaseProducts(@NonNull ProductsResponse productResponse) {
        Log.v(TAG, "создания листа BaseProduct"); //

        List<BaseProduct> products = new ArrayList<>();

        //products.addAll(accounts);
        products.addAll(productResponse.getCreditCards());
        products.addAll(productResponse.getCredits());
        products.addAll(productResponse.getDebitCards());
        products.addAll(productResponse.getDeposits());
        return products;
    }
}
