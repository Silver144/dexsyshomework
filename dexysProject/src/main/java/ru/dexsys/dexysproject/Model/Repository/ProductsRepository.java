package ru.dexsys.dexysproject.Model.Repository;


import android.support.annotation.NonNull;

import java.util.ArrayList;
import java.util.List;

import ru.dexsys.dexysproject.Model.Data.BaseProduct;

public class ProductsRepository {

    @NonNull
    private final List<BaseProduct> products;
    //private BankApi bankApi;

    public ProductsRepository() {
        //this.bankApi = bankApi;
        this.products = new ArrayList<>();
    }

    public void setProducts(@NonNull List<BaseProduct> products) {
        this.products.clear();
        this.products.addAll(products);
    }
/*
    @NonNull
    public Observable<List<BaseProduct>> getProducts() {
        return bankApi.getProducts()
                .map(response -> addListsToBaseProducts(response.getResult()));
    }
*/
    @NonNull
    public List<BaseProduct> getSavedProducts() {
        return products;
    }
}
