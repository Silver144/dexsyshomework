package ru.dexsys.dexysproject.Model.Data;

import com.google.gson.annotations.SerializedName;

import java.util.Date;

public class Payment extends BaseProduct {

    @SerializedName("paymentDate")
    private Date paymentDate;
    @SerializedName("paymentAmount")
    private Double paymentAmount;

    private Payment(Builder builder) {
        setPaymentDate(builder.paymentDate);
        setPaymentAmount(builder.paymentAmount);
        setStatus(builder.status);
    }

    public static final class Builder {
        private Date paymentDate;
        private Double paymentAmount;
        private String status;

        public Builder() {
        }

        public Builder paymentDate(Date val) {
            paymentDate = val;
            return this;
        }
        public Builder paymentAmount(Double val) {
            paymentAmount = val;
            return this;
        }
        public Builder status(String val) {
            status = val;
            return this;
        }
        public Payment build() {
            return new Payment(this);
        }
    }

    public Date getPaymentDate() {
        return paymentDate;
    }
    public void setPaymentDate(Date paymentDate) {
        this.paymentDate = paymentDate;
    }

    public Double getPaymentAmount() {
        return paymentAmount;
    }
    public void setPaymentAmount(Double paymentAmount) {
        this.paymentAmount = paymentAmount;
    }
}
