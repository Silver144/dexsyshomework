package ru.dexsys.dexysproject.Model.Data;

import com.google.gson.annotations.SerializedName;

import java.util.Date;

public class Deposit extends BaseProduct {

    @SerializedName("closingDate")
    private Date closingDate;
    @SerializedName("percent")
    private Double percent;

    private Deposit(Builder builder) {
        setName(builder.name);
        setContractNumber(builder.contractNumber);
        setOpenedDate(builder.openedDate);
        setAmount(builder.amount);
        setCurrency(builder.currency);
        setStatus(builder.status);
        setClosingDate(builder.closingDate);
        setPercent(builder.percent);
    }

    public static final class Builder {
        private String name;
        private Integer contractNumber;
        private Date openedDate;
        private Double amount;
        private String currency;
        private String status;
        private Date closingDate;
        private Double percent;

        public Builder() {
        }

        public Builder name(String val) {
            name = val;
            return this;
        }
        public Builder contractNumber(Integer val) {
            contractNumber = val;
            return this;
        }
        public Builder openedDate(Date val) {
            openedDate = val;
            return this;
        }
        public Builder amount(Double val) {
            amount = val;
            return this;
        }
        public Builder currency(String val) {
            currency = val;
            return this;
        }
        public Builder status(String val) {
            status = val;
            return this;
        }
        public Builder closingDate(Date val) {
            closingDate = val;
            return this;
        }
        public Builder percent(Double val) {
            percent = val;
            return this;
        }
        public Deposit build() {
            return new Deposit(this);
        }
    }

    public Date getClosingDate() {
        return closingDate;
    }
    public void setClosingDate(Date closingDate) {
        this.closingDate = closingDate;
    }

    public Double getPercent() {
        return percent;
    }
    public void setPercent(Double percent) {
        this.percent = percent;
    }
}
