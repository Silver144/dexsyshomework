package ru.dexsys.dexysproject.UI.Products.Items;

import java.util.Date;

import ru.dexsys.dexysproject.Formatters.PriceFormatter;
import ru.dexsys.dexysproject.Formatters.DateFormatter;

public class ClosedUnit {
    private String closedValue;
    private String closedDate;

    public String getClosedValue() {
        return closedValue;
    }
    public void setClosedValue(String value) {
        closedValue = value;
    }

    public String getClosedDate() {
        return closedDate;
    }
    public void setClosedDate(String date) {
        closedDate = date;
    }

    public ClosedUnit(double value, String currency, Date date) {
        closedValue = PriceFormatter.formatPrice(value, currency);
        closedDate = DateFormatter.formatDate(date);
    }
}
