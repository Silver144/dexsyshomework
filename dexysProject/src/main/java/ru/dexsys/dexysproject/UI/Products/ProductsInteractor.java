package ru.dexsys.dexysproject.UI.Products;

import android.support.annotation.NonNull;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import ru.dexsys.dexysproject.Model.Data.BaseProduct;
import ru.dexsys.dexysproject.Model.Repository.BaseProductConverter;
import ru.dexsys.dexysproject.Model.Repository.ProductsRepository;
import ru.dexsys.dexysproject.Parsing.BankApi;

public class ProductsInteractor {

    @NonNull
    private final BankApi bankApi;
    @NonNull
    private final BaseProductConverter baseProductConverter;
    @NonNull
    private final ProductsRepository productsRepository;

    @Inject
    public ProductsInteractor(@NonNull BankApi bankApi,
                              @NonNull BaseProductConverter baseProductConverter,
                              @NonNull ProductsRepository productsRepository) {
        this.bankApi = bankApi;
        this.baseProductConverter = baseProductConverter;
        this.productsRepository = productsRepository;
    }

    @NonNull
    public Single<List<BaseProduct>> getProducts() {
        return bankApi.getProducts()
                .map(response -> response.getResult())
                .map(result -> baseProductConverter.addListsToBaseProducts(result))
                .firstOrError() //превращаем в Single
                .doOnSuccess(baseProducts -> saveToRepository(baseProducts))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    private void saveToRepository(List<BaseProduct> baseProducts) {
        productsRepository.setProducts(baseProducts);
    }
}
