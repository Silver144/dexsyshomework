package ru.dexsys.dexysproject.UI;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;

public abstract class BaseFragment<P extends BasePresenter> extends Fragment {
    private static final String TAG = "FragLog";

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getPresenter().attachView((MvpView) this);
        Log.v(TAG, "BaseFragment: onCreate"); //
    }

    @Override
    public void onDestroyView() {
        getPresenter().detachView();
        super.onDestroyView();
    }

    protected abstract P getPresenter();
}
