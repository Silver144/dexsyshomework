package ru.dexsys.dexysproject.UI.Products.RecyclerView.ViewHolders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import ru.dexsys.dexysproject.R;

public class DepositVH extends RecyclerView.ViewHolder {

    private TextView depositTitle;
    private TextView depositValue;
    private TextView depositDate;

    public TextView getDepositTitle() {
        return depositTitle;
    }
    public TextView getDepositValue() {
        return depositValue;
    }
    public TextView getDepositDate() {
        return depositDate;
    }

    public DepositVH(View itemView) {
        super(itemView);
        depositTitle = itemView.findViewById(R.id.deposit_title);
        depositValue = itemView.findViewById(R.id.deposit_value);
        depositDate = itemView.findViewById(R.id.deposit_date);
    }
}
