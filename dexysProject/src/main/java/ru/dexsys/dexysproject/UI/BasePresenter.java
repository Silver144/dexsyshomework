package ru.dexsys.dexysproject.UI;

import android.support.annotation.NonNull;

import io.reactivex.disposables.CompositeDisposable;

public abstract class BasePresenter<C extends MvpView> {

    @NonNull
    protected final CompositeDisposable compositeDisposable = new CompositeDisposable();
    private C view;

    public void attachView(@NonNull C view) {
        this.view = view;
    }

    public void detachView() {
        view = null;
        if (!compositeDisposable.isDisposed()) {
            compositeDisposable.dispose();
        }
    }

    protected boolean isViewAttached() {
        return view != null;
    }

    protected C getView() {
        return view;
    }

}
