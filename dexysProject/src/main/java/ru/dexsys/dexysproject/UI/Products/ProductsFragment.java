package ru.dexsys.dexysproject.UI.Products;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import ru.dexsys.dexysproject.DexysProject;
import ru.dexsys.dexysproject.R;
import ru.dexsys.dexysproject.UI.BaseFragment;
import ru.dexsys.dexysproject.UI.Products.Items.BaseItem;
import ru.dexsys.dexysproject.UI.Products.RecyclerView.ProductAdapter;

public class ProductsFragment extends BaseFragment<ProductsPresenter> implements ProductsView {
    private static final String TAG = "FragLog";

    @Inject
    ProductsPresenter productsPresenter;

    @Override
    protected ProductsPresenter getPresenter() {
        return productsPresenter;
    }

    //@Inject
    //BankApi bankApi;
    //@Inject
    //ProductsRepository productsRepository;

    //private ArrayList<BaseItem> baseItems;
    @BindView(R.id.recycler_products)
    RecyclerView productsRecyclerView;
    //ProductsItemBuilder productsItemBuilder;
    private Unbinder unbinder;

    public ProductsFragment() {
        Log.v(TAG, "запуск конструктора ProductsFragment"); //
    }

    public static ProductsFragment newInstance() {
        Log.v(TAG, "Создание экземпляра ProductsFragment"); //
        return new ProductsFragment();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ((DexysProject) getActivity().getApplication()).getMainComponent().inject(this);
        Log.v(TAG, "ProductsFragment: onCreate"); //
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_products, container, false);
        //baseItems = new ArrayList<>();
        //productsItemBuilder = new ProductsItemBuilderImpl();
        //productsRecyclerView = rootView.findViewById(R.id.recycler_products);
        //LinearLayoutManager linearLayoutManager = new LinearLayoutManager(rootView.getContext()); //экземпляр LayoutManager
        //productsRecyclerView.setLayoutManager(linearLayoutManager);
        //Подписка:
        /*
        productsRepository.getProducts()
                .firstOrError() //превращаем в Single
                .doOnSuccess(baseProducts -> saveToRepository(baseProducts))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(baseProducts -> initializeAdapter(productsItemBuilder.build(baseProducts)),
                        throwable -> initializeAdapter(productsItemBuilder.build(productsRepository.getSavedProducts())));

        */Log.v(TAG, "ProductsFragment: onCreateView"); //
        return rootView;
    }

    @Override
    public void onViewCreated(View rootView, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(rootView, savedInstanceState);
        unbinder = ButterKnife.bind(this, rootView);
        initRecyclerView(rootView);
        productsPresenter.initState();
    }

    @Override
    public void showProducts(@NonNull List<BaseItem> baseItems) {
        ProductAdapter adapter = new ProductAdapter(baseItems);
        productsRecyclerView.setAdapter(adapter);
    }

    @Override
    public void showProductsEmptyList() {
        //отображаем сообщение об отсутствии продуктов
    }

    @Override
    public void showProductsError() {
        //отображаем ошибку
    }

    public void initRecyclerView(View rootView) {
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(rootView.getContext());
        productsRecyclerView.setLayoutManager(linearLayoutManager);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

/*
    private void saveToRepository(List<BaseProduct> baseProducts) {
        productsRepository.setProducts(baseProducts);
    }

    private void initializeAdapter(@NonNull List<BaseItem> baseItemsLocal) {
        this.baseItems = (ArrayList<BaseItem>) baseItemsLocal;
        ProductAdapter adapter = new ProductAdapter(baseItems);
        productsRecyclerView.setAdapter(adapter);
    }
*/
}
