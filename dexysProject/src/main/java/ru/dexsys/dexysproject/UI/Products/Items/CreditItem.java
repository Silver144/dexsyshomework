package ru.dexsys.dexysproject.UI.Products.Items;

import java.util.Date;

import ru.dexsys.dexysproject.Formatters.PriceFormatter;
import ru.dexsys.dexysproject.Formatters.DateFormatter;

public class CreditItem extends BaseItem {

    public CreditItem(String localItemTitle, Date localProductDate, double localProductValue, String localCurrency, double localProductPayment) {
        super.setItemTitle(localItemTitle);

        String formattedProductDate;
        formattedProductDate = DateFormatter.formatDate(localProductDate);
        super.setProductDate(formattedProductDate);

        String formattedProductValue;
        formattedProductValue = PriceFormatter.formatPrice(localProductValue, localCurrency);
        super.setProductValue(formattedProductValue);

        String formattedProductPayment;
        formattedProductPayment = PriceFormatter.formatPrice(localProductPayment, localCurrency);
        super.setProductPayment(formattedProductPayment);
    }

}
