package ru.dexsys.dexysproject.UI.Products.Items;

import ru.dexsys.dexysproject.Formatters.CardNumberFormatter;
import ru.dexsys.dexysproject.Formatters.PriceFormatter;

public class CardsItem extends BaseItem {

    public CardsItem(String localItemTitle, String localProductNumber, double localProductValue, String localCurrency) {
        super.setItemTitle(localItemTitle);

        String formattedProductNumber;
        formattedProductNumber = CardNumberFormatter.formatCardNumber(localProductNumber);
        super.setProductNumber(formattedProductNumber);

        String formattedProductValue;
        formattedProductValue = PriceFormatter.formatPrice(localProductValue, localCurrency);
        super.setProductValue(formattedProductValue);
    }
}
