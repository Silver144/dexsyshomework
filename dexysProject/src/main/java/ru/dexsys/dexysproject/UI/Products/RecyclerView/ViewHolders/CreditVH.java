package ru.dexsys.dexysproject.UI.Products.RecyclerView.ViewHolders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import ru.dexsys.dexysproject.R;

public class CreditVH extends RecyclerView.ViewHolder {

    private TextView creditTitle;
    private TextView creditValue;
    private TextView creditDate;
    private TextView creditPayment;

    public TextView getCreditTitle() {
        return creditTitle;
    }
    public TextView getCreditValue() {
        return creditValue;
    }
    public TextView getCreditDate() {
        return creditDate;
    }
    public TextView getCreditPayment() {
        return creditPayment;
    }

    public CreditVH(View itemView) {
        super(itemView);
        creditTitle = itemView.findViewById(R.id.credit_title);
        creditValue = itemView.findViewById(R.id.credit_value);
        creditDate = itemView.findViewById(R.id.credit_date);
        creditPayment = itemView.findViewById(R.id.credit_payment);
    }
}
