package ru.dexsys.dexysproject.UI.Products.RecyclerView.ViewHolders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import ru.dexsys.dexysproject.R;

public class TitleVH extends RecyclerView.ViewHolder {

    private TextView textTitle;

    public TextView getTextTitle() {
        return textTitle;
    }

    public TitleVH(View itemView) {
        super(itemView);
        textTitle = itemView.findViewById(R.id.text_title);
    }
}
