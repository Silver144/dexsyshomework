package ru.dexsys.dexysproject.UI.Products;

import android.support.annotation.NonNull;

import java.util.List;

import ru.dexsys.dexysproject.UI.MvpView;
import ru.dexsys.dexysproject.UI.Products.Items.BaseItem;

public interface ProductsView extends MvpView {

    void showProducts(@NonNull List<BaseItem> baseItems);
    void showProductsEmptyList();
    void showProductsError();
}
