package ru.dexsys.dexysproject.UI.Products.RecyclerView;

import android.support.annotation.IntDef;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.List;

import ru.dexsys.dexysproject.R;
import ru.dexsys.dexysproject.UI.Products.Items.BaseItem;
import ru.dexsys.dexysproject.UI.Products.Items.CardsItem;
import ru.dexsys.dexysproject.UI.Products.Items.CreditItem;
import ru.dexsys.dexysproject.UI.Products.Items.DepositItem;
import ru.dexsys.dexysproject.UI.Products.Items.TitleItem;
import ru.dexsys.dexysproject.UI.Products.RecyclerView.ViewHolders.CardsVH;
import ru.dexsys.dexysproject.UI.Products.RecyclerView.ViewHolders.CreditVH;
import ru.dexsys.dexysproject.UI.Products.RecyclerView.ViewHolders.DepositVH;
import ru.dexsys.dexysproject.UI.Products.RecyclerView.ViewHolders.TitleVH;

public class ProductAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private static final String TAG = "FragLog";

    private List<BaseItem> adapterBaseItems;

    public ProductAdapter(@NonNull List<BaseItem> items) {
        adapterBaseItems = items;
        Log.v(TAG, "ProductAdapter: конструктор"); //
    }

    @IntDef({ItemType.TITLE, ItemType.CARDS, ItemType.DEPOSIT, ItemType.CREDIT})
    @Retention(RetentionPolicy.SOURCE)
    public @interface ItemType {
        int TITLE = 0;
        int CARDS = 1;
        int DEPOSIT = 2;
        int CREDIT = 3;
        //
        //int CLOSED = 4;
        //

    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View rootView;
        Log.v(TAG, "ProductAdapter: onCreateViewHolder"); //
        switch (viewType) {
            case ItemType.TITLE:
                rootView = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.layout_products_title, parent, false);
                return new TitleVH(rootView);
            case ItemType.CARDS:
                rootView = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.layout_products_cards, parent, false);
                return new CardsVH(rootView);
            case ItemType.DEPOSIT:
                rootView = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.layout_products_deposit, parent, false);
                return new DepositVH(rootView);
            case ItemType.CREDIT:
                rootView = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.layout_products_credit, parent, false);
                return new CreditVH(rootView);
            default:
                return null;

        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        BaseItem item = adapterBaseItems.get(position);
        Log.v(TAG, "ProductAdapter: onBindViewHolder"); //
        if (item instanceof TitleItem) {
            TitleItem titleItem = (TitleItem) item;
            TitleVH titleVH = (TitleVH) holder;
            titleVH.getTextTitle().setText(titleItem.getItemTitle());
        } else if (item instanceof CardsItem) {
            CardsItem cardsItem = (CardsItem) item;
            CardsVH cardsVH = (CardsVH) holder;
            cardsVH.getCardsTitle().setText(cardsItem.getItemTitle());
            cardsVH.getCardsValue().setText(cardsItem.getProductValue());
            cardsVH.getCardsNumber().setText(cardsItem.getProductNumber());
        } else if (item instanceof DepositItem) {
            DepositItem depositItem = (DepositItem) item;
            DepositVH depositVH = (DepositVH) holder;
            depositVH.getDepositTitle().setText(depositItem.getItemTitle());
            depositVH.getDepositValue().setText(depositItem.getProductValue());
            depositVH.getDepositDate().setText(depositItem.getProductDate());
        } else if (item instanceof CreditItem) {
            CreditItem creditItem = (CreditItem) item;
            CreditVH creditVH = (CreditVH) holder;
            creditVH.getCreditTitle().setText(creditItem.getItemTitle());
            creditVH.getCreditValue().setText(creditItem.getProductValue());
            creditVH.getCreditDate().setText(creditItem.getProductDate());
            creditVH.getCreditPayment().setText(creditItem.getProductPayment());
        }
    }

    @Override
    public int getItemViewType(int position) {
        Log.v(TAG, "ProductAdapter: getItemViewType"); //
        if (adapterBaseItems.get(position) instanceof TitleItem) {
            return ItemType.TITLE;
        } else if (adapterBaseItems.get(position) instanceof CardsItem) {
            return ItemType.CARDS;
        } else if (adapterBaseItems.get(position) instanceof DepositItem) {
            return ItemType.DEPOSIT;
        } else if (adapterBaseItems.get(position) instanceof CreditItem) {
            return ItemType.CREDIT;
        }
        return -1;
    }

    @Override
    public int getItemCount() {
        Log.v(TAG, "ProductAdapter: getItemCount"); //
        return adapterBaseItems.size();
    }



}
