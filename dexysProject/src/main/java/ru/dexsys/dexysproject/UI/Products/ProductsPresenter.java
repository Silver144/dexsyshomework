package ru.dexsys.dexysproject.UI.Products;

import android.support.annotation.NonNull;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.disposables.Disposable;
import ru.dexsys.dexysproject.Model.Data.BaseProduct;
import ru.dexsys.dexysproject.UI.BasePresenter;

public class ProductsPresenter extends BasePresenter<ProductsView> {

    @NonNull
    private final ProductsInteractor productsInteractor;
    @NonNull
    private final ProductsItemBuilder productsItemBuilder;

    @Inject
    public ProductsPresenter(@NonNull ProductsInteractor productsInteractor,
                             @NonNull ProductsItemBuilder productsItemBuilder) {
        this.productsInteractor = productsInteractor;
        this.productsItemBuilder = productsItemBuilder;
    }

    public void initState() {
        loadProducts();
    }

    private void loadProducts() {
        Disposable disposable = productsInteractor.getProducts()
                .subscribe(products -> onGetProducts(products),
                        throwable -> onGetProductsError(throwable));
        compositeDisposable.add(disposable);
    }

    private void onGetProducts(@NonNull List<BaseProduct> products) {
        if (isViewAttached()) {
            getView().showProducts(productsItemBuilder.build(products));
        }
    }

    private void onGetProductsError(Throwable throwable) {
        if (isViewAttached()) {
            getView().showProductsError();
        }
    }

    public void onReloadProductsClick() {
        loadProducts();
    }
}
