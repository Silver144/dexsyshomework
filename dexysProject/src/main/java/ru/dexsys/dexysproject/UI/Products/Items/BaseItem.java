package ru.dexsys.dexysproject.UI.Products.Items;

public abstract class BaseItem {
    private String itemTitle;
    private String productDate;
    private String productNumber;
    private String productValue;
    private String productPayment;


    public void setItemTitle(String itemTitle) {
        this.itemTitle = itemTitle;
    }
    public String getItemTitle() {
        return itemTitle;
    }

    public void setProductDate(String productDate) {
        this.productDate = productDate;
    }
    public String getProductDate() {
        return productDate;
    }

    public void setProductNumber(String productNumber) {
        this.productNumber = productNumber;
    }
    public String getProductNumber() {
        return productNumber;
    }

    public void setProductValue(String productValue) {
        this.productValue = productValue;
    }
    public String getProductValue() {
        return productValue;
    }

    public void setProductPayment(String productPayment) {
        this.productPayment = productPayment;
    }
    public String getProductPayment() {
        return productPayment;
    }

}
