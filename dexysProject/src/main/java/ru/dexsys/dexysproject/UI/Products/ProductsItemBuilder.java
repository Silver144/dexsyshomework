package ru.dexsys.dexysproject.UI.Products;

import android.support.annotation.NonNull;

import java.util.ArrayList;
import java.util.List;

import ru.dexsys.dexysproject.UI.Products.Items.BaseItem;
import ru.dexsys.dexysproject.Model.Data.BaseProduct;

public interface ProductsItemBuilder {

    @NonNull
    List<BaseItem> build(@NonNull List<BaseProduct> products);
    ArrayList<BaseItem> getProductList();

}
