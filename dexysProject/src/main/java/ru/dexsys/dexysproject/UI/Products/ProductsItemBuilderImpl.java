package ru.dexsys.dexysproject.UI.Products;

import android.support.annotation.NonNull;

import java.util.ArrayList;
import java.util.List;

import ru.dexsys.dexysproject.Model.Data.BaseProduct;
import ru.dexsys.dexysproject.Model.Data.Credit;
import ru.dexsys.dexysproject.Model.Data.CreditCard;
import ru.dexsys.dexysproject.Model.Data.DebitCard;
import ru.dexsys.dexysproject.Model.Data.Deposit;
import ru.dexsys.dexysproject.UI.Products.Items.BaseItem;
import ru.dexsys.dexysproject.UI.Products.Items.CardsItem;
import ru.dexsys.dexysproject.UI.Products.Items.CreditItem;
import ru.dexsys.dexysproject.UI.Products.Items.DepositItem;
import ru.dexsys.dexysproject.UI.Products.Items.TitleItem;

public class ProductsItemBuilderImpl implements ProductsItemBuilder {

    private ArrayList<BaseItem> items;

    public ProductsItemBuilderImpl() {
        //конструктор
    }

    public ArrayList<BaseItem> getProductList() {
        return items;
    }

    @NonNull
    @Override
    public List<BaseItem> build(@NonNull List<BaseProduct> products) {
        items = new ArrayList<>();

        buildDeposits(products, items);
        buildCredits(products, items);
        buildCreditCards(products, items);
        buildDebitCards(products, items);
        return items;
    }

    private void buildDeposits(@NonNull List<BaseProduct> products,
                               @NonNull List<BaseItem> items) {
        boolean isTitleAdded = false;
        for (BaseProduct product : products) {
            if (product instanceof Deposit) {
                if (!product.getStatus().equals("closed")) {
                    if (!isTitleAdded) {
                        items.add(new TitleItem("Вклады"));
                        isTitleAdded = true;
                    }
                    items.add(new DepositItem(product.getName(), product.getOpenedDate(), product.getAmount(), product.getCurrency()));
                }
            }
        }
    }

    private void buildCredits(@NonNull List<BaseProduct> products,
                               @NonNull List<BaseItem> items) {
        boolean isTitleAdded = false;
        for (BaseProduct product : products) {
            if (product instanceof Credit) {
                if (!product.getStatus().equals("closed")) {
                    if (!isTitleAdded) {
                        items.add(new TitleItem("Кредиты"));
                        isTitleAdded = true;
                    }
                    items.add(new CreditItem(product.getName(), ((Credit) product).getNextPaymentDate(), product.getAmount(), product.getCurrency(), ((Credit) product).getNextPaymentAmount()));
                }
            }
        }
    }

    private void buildCreditCards(@NonNull List<BaseProduct> products,
                              @NonNull List<BaseItem> items) {
        boolean isTitleAdded = false;
        for (BaseProduct product : products) {
            if (product instanceof CreditCard) {
                if (!product.getStatus().equals("closed")) {
                    if (!isTitleAdded) {
                        items.add(new TitleItem("Кредитные карты"));
                        isTitleAdded = true;
                    }
                    items.add(new CardsItem(product.getName(), ((CreditCard) product).getCardNumber(), product.getAmount(), product.getCurrency()));
                }
            }
        }
    }

    private void buildDebitCards(@NonNull List<BaseProduct> products,
                            @NonNull List<BaseItem> items) {
        boolean isTitleAdded = false;
        for (BaseProduct product : products) {
            if (product instanceof DebitCard) {
                if (!product.getStatus().equals("closed")) {
                    if (!isTitleAdded) {
                        items.add(new TitleItem("Дебетовые карты"));
                        isTitleAdded = true;
                    }
                    items.add(new CardsItem(product.getName(), ((DebitCard) product).getCardNumber(), product.getAmount(), product.getCurrency()));
                }
            }
        }
    }
}
