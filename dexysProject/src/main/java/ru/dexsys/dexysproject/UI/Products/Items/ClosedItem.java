package ru.dexsys.dexysproject.UI.Products.Items;

import java.util.ArrayList;

public class ClosedItem extends BaseItem {
    private ArrayList<ClosedUnit> fields;

    public ClosedItem(ArrayList<ClosedUnit> titles) {
        fields = titles;
    }

    public ArrayList<ClosedUnit> getFields() {
        return fields;
    }
    public void setFields(ArrayList<ClosedUnit> titles) {
        this.fields = titles;
    }
}

