package ru.dexsys.dexysproject.UI.Products.RecyclerView.ViewHolders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import ru.dexsys.dexysproject.R;

public class CardsVH extends RecyclerView.ViewHolder {

    private TextView cardsTitle;
    private TextView cardsValue;
    private TextView cardsNumber;

    public TextView getCardsTitle() {
        return cardsTitle;
    }
    public TextView getCardsValue() {
        return cardsValue;
    }
    public TextView getCardsNumber() {
        return cardsNumber;
    }

    public CardsVH(View itemView) {
        super(itemView);
        cardsTitle = itemView.findViewById(R.id.cards_title);
        cardsValue = itemView.findViewById(R.id.cards_value);
        cardsNumber = itemView.findViewById(R.id.cards_number);
    }
}
