package ru.dexsys.dexysproject.Parsing;

import com.google.gson.annotations.SerializedName;

public class ApiResponse <T> {

    @SerializedName("resultCode")
    private int resultCode;
    @SerializedName("error")
    private String error;
    @SerializedName("result")
    private T result;

    public int getResultCode() {
        return resultCode;
    }
    public void setResultCode(int resultCode) {
        this.resultCode = resultCode;
    }

    public String getError() {
        return error;
    }
    public void setError(String error) {
        this.error = error;
    }

    public T getResult() {
        return result;
    }
    public void setResult(T result) {
        this.result = result;
    }
}
