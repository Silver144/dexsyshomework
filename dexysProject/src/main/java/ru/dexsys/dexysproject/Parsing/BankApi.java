package ru.dexsys.dexysproject.Parsing;


import io.reactivex.Observable;
import retrofit2.http.GET;

public interface BankApi {
    @GET("/v2/5b21590730000091265c7459")
    Observable<ApiResponse<ProductsResponse>> getProducts();
}
