package ru.dexsys.dexysproject.Parsing;

import com.google.gson.annotations.SerializedName;

import java.util.List;

import ru.dexsys.dexysproject.Model.Data.Account;
import ru.dexsys.dexysproject.Model.Data.Credit;
import ru.dexsys.dexysproject.Model.Data.CreditCard;
import ru.dexsys.dexysproject.Model.Data.DebitCard;
import ru.dexsys.dexysproject.Model.Data.Deposit;

public class ProductsResponse {
    private static final String TAG = "FragLog";

    @SerializedName("accounts")
    private List<Account> accounts;
    @SerializedName("deposits")
    private List<Deposit> deposits;
    @SerializedName("creditCards")
    private List<CreditCard> creditCards;
    @SerializedName("debitCards")
    private List<DebitCard> debitCards;
    @SerializedName("credits")
    private List<Credit> credits;

    public List<Account> getAccounts() {
        return accounts;
    }
    public void setAccounts(List<Account> accounts) {
        this.accounts = accounts;
    }

    public List<Deposit> getDeposits() {
        return deposits;
    }
    public void setDeposits(List<Deposit> deposits) {
        this.deposits = deposits;
    }

    public List<CreditCard> getCreditCards() {
        return creditCards;
    }
    public void setCreditCards(List<CreditCard> creditCards) {
        this.creditCards = creditCards;
    }

    public List<DebitCard> getDebitCards() {
        return debitCards;
    }
    public void setDebitCards(List<DebitCard> debitCards) {
        this.debitCards = debitCards;
    }

    public List<Credit> getCredits() {
        return credits;
    }
    public void setCredits(List<Credit> credits) {
        this.credits = credits;
    }

}
