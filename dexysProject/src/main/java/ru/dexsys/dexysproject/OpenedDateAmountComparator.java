package ru.dexsys.dexysproject;

import java.util.Comparator;

import ru.dexsys.dexysproject.Model.Data.BaseProduct;

public class OpenedDateAmountComparator implements Comparator<BaseProduct> {
    @Override
    public int compare(BaseProduct b1, BaseProduct b2) {
        int depositOpenedDateComparison = b1.getOpenedDate().compareTo(b2.getOpenedDate());
        return depositOpenedDateComparison == 0 ? b1.getAmount().compareTo(b2.getAmount())
                : depositOpenedDateComparison;
    }
    //todo разобраться в устрйостве компаратора
}
