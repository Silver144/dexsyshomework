package ru.dexsys.dexysproject.Dagger2;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import ru.dexsys.dexysproject.Model.Repository.BaseProductConverter;
import ru.dexsys.dexysproject.UI.Products.ProductsItemBuilder;
import ru.dexsys.dexysproject.UI.Products.ProductsItemBuilderImpl;

@Module
public class UtilsModule {

    @Provides
    @Singleton
    public BaseProductConverter provideBaseProductConverter() {
        return new BaseProductConverter();
    }

    @Provides
    @Singleton
    public ProductsItemBuilder provideProductsItemBuilder() {
        return new ProductsItemBuilderImpl();
    }
}
