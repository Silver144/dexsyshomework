package ru.dexsys.dexysproject.Dagger2;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import ru.dexsys.dexysproject.Model.Repository.ProductsRepository;

@Module
public class ProductsRepositoryModule {

    @Provides
    @Singleton
    ProductsRepository provideProductsRepository() {
        return new ProductsRepository();
    }
}
