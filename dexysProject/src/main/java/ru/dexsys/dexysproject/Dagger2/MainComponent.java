package ru.dexsys.dexysproject.Dagger2;

import javax.inject.Singleton;

import dagger.Component;
import ru.dexsys.dexysproject.UI.Products.ProductsFragment;

@Singleton
@Component(modules = {
        NetworkModule.class,
        ProductsRepositoryModule.class,
        UtilsModule.class,
        ProductsPresenterModule.class,
        ProductsInteractorModule.class
})

public interface MainComponent {

    void inject(ProductsFragment productsFragment);
}
