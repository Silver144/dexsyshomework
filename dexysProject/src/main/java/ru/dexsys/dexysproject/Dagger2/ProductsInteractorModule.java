package ru.dexsys.dexysproject.Dagger2;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import ru.dexsys.dexysproject.Model.Repository.BaseProductConverter;
import ru.dexsys.dexysproject.Model.Repository.ProductsRepository;
import ru.dexsys.dexysproject.Parsing.BankApi;
import ru.dexsys.dexysproject.UI.Products.ProductsInteractor;

@Module
public class ProductsInteractorModule {

    @Provides
    @Singleton
    public ProductsInteractor provideProductsInteractor(BankApi bankApi,
                                                        BaseProductConverter baseProductConverter,
                                                        ProductsRepository productsRepository) {
        return new ProductsInteractor(bankApi, baseProductConverter, productsRepository);
    }
}
