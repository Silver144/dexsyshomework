package ru.dexsys.dexysproject.Dagger2;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import ru.dexsys.dexysproject.UI.Products.ProductsInteractor;
import ru.dexsys.dexysproject.UI.Products.ProductsItemBuilder;
import ru.dexsys.dexysproject.UI.Products.ProductsPresenter;

@Module
public class ProductsPresenterModule {
    @Provides
    @Singleton
    public ProductsPresenter provideProductsPresenter(ProductsInteractor productsInteractor, ProductsItemBuilder productsItemBuilder) {
        return new ProductsPresenter(productsInteractor, productsItemBuilder);
    }
}
