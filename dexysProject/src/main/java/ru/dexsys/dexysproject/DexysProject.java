package ru.dexsys.dexysproject;

import android.app.Application;
import android.support.annotation.NonNull;
import android.util.Log;

import ru.dexsys.dexysproject.Dagger2.DaggerMainComponent;
import ru.dexsys.dexysproject.Dagger2.MainComponent;
import ru.dexsys.dexysproject.Dagger2.NetworkModule;
import ru.dexsys.dexysproject.Dagger2.ProductsInteractorModule;
import ru.dexsys.dexysproject.Dagger2.ProductsPresenterModule;
import ru.dexsys.dexysproject.Dagger2.ProductsRepositoryModule;
import ru.dexsys.dexysproject.Dagger2.UtilsModule;

public class DexysProject extends Application {
    private static final String TAG = "FragLog";

    private MainComponent mainComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        Log.v(TAG, "DexysProject: onCreate"); //
        initDagger();
        Log.v(TAG, "DexysProject: initDagger"); //
    }

    private void initDagger() {
        mainComponent = DaggerMainComponent.builder()
                .networkModule(new NetworkModule()) //TODO: убрать хардкодинг http://www.mocky.io
                .productsRepositoryModule(new ProductsRepositoryModule())
                .utilsModule(new UtilsModule())
                .productsPresenterModule(new ProductsPresenterModule())
                .productsInteractorModule(new ProductsInteractorModule())
                //дописывать сюда все подключаемые модули
                .build();
    }

    @NonNull
    public MainComponent getMainComponent() {
        return mainComponent;
    }

}
